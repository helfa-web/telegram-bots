#!/bin/bash
set -Eeuo pipefail

if [ ! -z ${MONGO_GLOBAN_PASSWORD_FILE+x} ]; then
  MONGO_GLOBAN_PASSWORD=$(cat ${MONGO_GLOBAN_PASSWORD_FILE})
fi

if [ "$MONGO_GLOBAN_USERNAME" ] && [ "$MONGO_GLOBAN_PASSWORD" ]; then
  "${mongo[@]}" -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD" --authenticationDatabase "$rootAuthDatabase" "$MONGO_INITDB_DATABASE" <<-EOJS
    db.createUser({
      user: $(_js_escape "$MONGO_GLOBAN_USERNAME"),
      pwd: $(_js_escape "$MONGO_GLOBAN_PASSWORD"),
      roles: [{ role: "readWrite", db: $(_js_escape "$MONGO_INITDB_DATABASE")}]
    });
EOJS
fi

