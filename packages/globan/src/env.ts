import { config, DotenvConfigOutput } from 'dotenv';

export interface BotEnv {
  PACKAGE_NAME: string;
  APP_NAME?: string;
  ADMIN?: number;
  MONGO?: string;
  TOKEN?: string;
  DEBUG?: string[];
  [index: string]: unknown;
}

const PACKAGE_NAME = require('../package.json').name;
const env: BotEnv = {
  APP_NAME: '--APP_NAME unset--',
  PACKAGE_NAME
};

const castValue = (value: string) => {
  switch (true) {
    // boolean
    case value === 'false':
      return false;
    case value === 'true':
      return true;
    // int number
    case /^\d+$/.test(value):
      return parseInt(value, 10);
    // noop
    default:
      return value;
  }
};

const { parsed }: DotenvConfigOutput = config();

for (const key in parsed) {
  if (key === 'DEBUG') {
    env[key] = parsed[key].split(',');
  }
  const cast = castValue(parsed[key]);
  if (cast) {
    env[key] = cast;
  }
}

export default env;
