import { privateOnly, superAdminOnly } from '#middlewares/access';
import { BotContext } from '#middlewares/context';
import { Composer } from 'grammy';

export const start = new Composer<BotContext>();

// React to /start command
start.command('start', privateOnly, superAdminOnly, async (ctx) => {
  ctx.reply('Welcome! Up and running.');
});
