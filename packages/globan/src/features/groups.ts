import { privateOnly, superAdminOnly } from '#middlewares/access';
import { BotContext } from '#middlewares/context';
import { parseCommand, ParsedCommandContext } from '#middlewares/parseCommand';
import { ChatModel } from '#models/chat';
import { getCommand } from '#root/commands';
import createLogger from '#root/logging';
import { Composer } from 'grammy';
import { Chat } from 'grammy/out/platform';

const log = createLogger('groups');

const getSuperGroup = async (ctx: BotContext, chatId: string) => {
  try {
    return (await ctx.api.getChat(chatId)) as Chat.SupergroupChat;
  } catch (err) {
    // error_code === 400 might be something else
    if (err.description && err.description.includes('chat not found')) {
      ctx.reply(`No chat with id or name ${chatId}`);
      return;
    }
    throw err;
  }
};

export const groups = new Composer<BotContext>();

// React to /groups command
groups.command(
  'groups',
  superAdminOnly,
  privateOnly,
  parseCommand,
  async (ctx: ParsedCommandContext) => {
    const option = ctx.parsedCommand?.option || 'list';
    let chat;
    switch (option) {
      // just dump 'em all, for now
      case 'list':
        const chats = await ChatModel.find({ allowed: true });
        const message = `I can be added to ${
          chats.length
        } groups:\n\nName (id)\n\n${chats.map(
          (doc) => `@${doc.name} (${doc.id})`
        )}`;
        return ctx.reply(message);
      // allow - add new groups with or set allowed=true on known groups
      case 'allow':
        if (!ctx.parsedCommand?.parameters?.length) {
          return ctx.reply('This /groups option requires a parameter');
        }
        const allowChatId = ctx.parsedCommand.parameters[0];
        // get group
        chat = await getSuperGroup(ctx, allowChatId);
        if (!chat) {
          return;
        }
        // create Chat db object
        const { doc: dbChat } = await ChatModel.findOrCreate({
          id: chat.id
        });

        dbChat.name = chat.username;
        dbChat.allowed = true;
        await dbChat.save();

        log.info(`Allowing @${dbChat.name}:${dbChat.id}`);
        return ctx.reply(`I'm now allowing to be added to @${dbChat.name}`);
      // leave - sets allowed=false and bot leaves group
      case 'leave':
        if (!ctx.parsedCommand?.parameters?.length) {
          return ctx.reply('This /groups option requires a parameter');
        }
        // get group
        const leaveChatId = ctx.parsedCommand.parameters[0];
        chat = await getSuperGroup(ctx, leaveChatId);
        if (!chat) {
          return;
        }
        // set allowed=false on chat in DB
        const updated = await ChatModel.findOneAndUpdate(
          { id: chat.id, allowed: true },
          { allowed: false },
          { new: true }
        );

        if (updated) {
          // notify group of this action
          await ctx.api.sendMessage(
            updated.id,
            `@${ctx.from?.username} has revoked your access to this bot!\n\nI will leave now. Good Bye!`
          );
          // actually leave the group
          await ctx.api.leaveChat(chat.id);

          log.info(`Removed @${updated.name}:${updated.id} from allowed list`);
          return ctx.reply(
            `I left @${updated.name} and won't allow them to add me again.`
          );
        }
        return ctx.reply(
          `@${chat.username || '???'} (${
            chat.id
          }) is not allowed to add me. Nothing has changed.`
        );
      // view this as a general /help alias
      default:
        const msg = getCommand('groups')?.help;
        if (!msg) {
          return;
        }
        return ctx.reply(msg);
    }
  }
);

// Safeguard group joining
// - checks whether group that bot has been added to
//   has allowed=true
groups.on(':new_chat_members:is_bot', async (ctx) => {
  // check whether it's even me that joined
  const itsMe = ctx.msg.new_chat_members.some((cm) => cm.id === ctx.me.id);
  if (!itsMe) {
    return;
  }

  // do I want to be here?
  const allowedChat = await ChatModel.findOne({
    id: ctx.chat.id,
    allowed: true
  });

  if (!allowedChat) {
    await ctx.reply('You can not just add me. Sorry.');
    return ctx.api.leaveChat(ctx.chat.id);
  }

  allowedChat.joined = true;
  await allowedChat.save();
  return ctx.reply('Hi! :)');
});
