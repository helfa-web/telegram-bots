import { superAdminOnly, superGroupOnly } from '#middlewares/access';
import { BotCommandContext, BotContext } from '#middlewares/context';
import { BanAction, BannedUser, BannedUserModel } from '#models/ban';
import { getCommand } from '#root/commands';
import { createLogger } from '#root/logging';
import { Composer, MiddlewareFn } from 'grammy';

const log = createLogger('ban');

const applyToSenderGroup = async (
  ctx: BotContext,
  bannedUser: BannedUser,
  mode: boolean
) => {
  log.debug(mode);
  const fn = mode ? ctx.banChatMember.bind(ctx) : ctx.unbanChatMember.bind(ctx);
  try {
    await fn(bannedUser.id);
    return await ctx.reply(
      `${bannedUser.toIdString()} is now ${
        !mode ? 'un' : ''
      }banned from your entire group network.`
    );
  } catch (err) {
    if (err.description.includes("can't remove chat owner")) {
      log.warn(
        `Attempt to ${
          !mode ? 'un' : ''
        }ban group owner ${bannedUser.toIdString()} at ${ctx.chat?.id}`
      );
      return ctx.reply('Group owners can not be banned!');
    }
    throw err;
  }
};

export const ban = new Composer<BotContext>();

const handleCommand: MiddlewareFn<BotCommandContext> = async (ctx) => {
  if (!ctx.from) {
    throw new ReferenceError('ctx.from must be set here!');
  }

  // No idea how to get the required user id without tracking
  // all interactions of users and hoping that a possible mention
  // matches one of them to then get the id from the DB document.
  // => use replies to identify user to ban

  const commandMatch = ctx.msg.text.match(/^\/((?:un)?ban)/);
  log.debug(commandMatch);
  if (!commandMatch) {
    log.error('Command route hit without commandMatch');
    return;
  }
  const command = commandMatch[1];
  // User will be banned if true (/ban)
  // User will be unbanned if false (/unban - 'cause regex above matches nothing else)
  const targetState = command === 'ban';
  const replyToId = ctx.msg.reply_to_message?.from?.id;

  // send help message when used without replying to someone
  if (!replyToId) {
    const msg = getCommand(command)?.help;
    log.debug(msg);
    if (!msg) {
      return await ctx.reply(`No help for /${command} found.`);
    }
    return await ctx.reply(msg);
  }

  const { user: chatMember } = await ctx.getChatMember(replyToId);

  const banAction = new BanAction(targetState, ctx.chat, ctx.from);

  // get a ban record for the user
  const { created, doc: bannedUser } = await BannedUserModel.findOrCreate(
    {
      id: chatMember.id
    },
    {
      bans: [banAction],
      username: chatMember.username
    }
  );

  log.info(
    `@${ctx.from.username}(${
      ctx.from.id
    }) ${command}s ${bannedUser.toIdString()} in ${ctx.chat.id}`
  );

  // created with this banAction; just ban and done
  if (created) {
    return await applyToSenderGroup(ctx, bannedUser, targetState);
  }

  const needsUpdate = bannedUser.bans[0].banned !== targetState;
  if (needsUpdate) {
    log.warn(
      `Not updating BannedUser ${bannedUser.id} with last bans.banned === ${targetState}`
    );
  } else {
    // if user already has a record, add the new banAction
    bannedUser.bans.push(banAction);
    await bannedUser.save();
  }

  return await applyToSenderGroup(ctx, bannedUser, targetState);
};

// React to /ban command
ban.command(
  ['ban', 'unban'],
  superAdminOnly,
  superGroupOnly,
  //groupAdminOnly,
  handleCommand
);
