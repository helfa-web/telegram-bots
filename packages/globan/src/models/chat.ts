import {
  defaultClasses,
  getModelForClass,
  plugin,
  prop
} from '@typegoose/typegoose';
import mongoose from 'mongoose';
// https://github.com/TypeStrong/ts-node/issues/138
// @ts-ignore
import findOrCreate from 'mongoose-findorcreate';

export enum Language {
  ENGLISH = 'en',
  GERMAN = 'de'
}

@plugin(findOrCreate)
export class Chat extends defaultClasses.FindOrCreate {
  @prop({ required: true, index: true, unique: true })
  id!: number;
  @prop({ nullable: true })
  name?: string;
  @prop({ default: false })
  allowed?: boolean;
  @prop({ default: false })
  joined?: boolean;
  @prop({ enum: Language, default: Language.GERMAN })
  language?: Language;
  @prop({ default: 60 })
  timeGiven?: number;
}

// Get Chat model
export const ChatModel = getModelForClass(Chat, {
  existingMongoose: mongoose,
  schemaOptions: { timestamps: true, _id: false, id: false }
});
