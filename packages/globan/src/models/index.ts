import env from '#root/env';
import createLogger from '#root/logging';
import { setGlobalOptions, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';

const log = createLogger('db');

export const setupDatabase = async () => {
  log.info('Connecting to database');
  try {
    const loggerLevel =
      process.env.DEBUG && process.env.DEBUG.includes('mongo')
        ? 'debug'
        : undefined;
    console.log(loggerLevel);
    await mongoose.connect(env.MONGO as string, {
      serverSelectionTimeoutMS: 3000,
      connectTimeoutMS: 3000,
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      loggerLevel
    });
  } catch (err) {
    log.fatal(err.message);
    log.trace(err.reason);
    process.exit(1);
  }

  setGlobalOptions({
    options: {
      allowMixed: Severity.ALLOW
    }
  });
};
