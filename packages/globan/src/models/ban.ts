import {
  getModelForClass,
  modelOptions,
  plugin,
  prop
} from '@typegoose/typegoose';
import { FindOrCreate } from '@typegoose/typegoose/lib/defaultClasses';
import { Chat, User } from 'grammy/out/platform';
// https://github.com/TypeStrong/ts-node/issues/138
// @ts-ignore
import findOrCreate from 'mongoose-findorcreate';

/**
 * Represents one ban or unban action
 *
 * This is a subdocument without _id and for use in @ChatMemberBanned.bans[]
 *
 * @param ban   - Whether to ban or unban
 * @param chat  - Channel/Private/Group id where user got banned in
 * @param admin - Admin chat member that took the action
 */
@modelOptions({ schemaOptions: { _id: false, timestamps: true } })
export class BanAction {
  constructor(ban: boolean, { id: bannedIn }: Chat, { id: bannedBy }: User) {
    this.banned = ban;
    this.bannedIn = bannedIn;
    this.bannedBy = bannedBy;
  }
  @prop({ reuqired: true })
  public banned!: boolean;
  @prop({ required: true })
  public bannedBy!: number;
  @prop({ required: true })
  public bannedIn!: number;
}

/**
 * Represents one banned ChatMember
 *
 * @param member  - User to ban/unban
 * @param ban     - First BanAction to save for the member
 */
@plugin(findOrCreate)
@modelOptions({
  schemaOptions: { timestamps: true }
})
export class BannedUser extends FindOrCreate {
  @prop({ required: true, index: true, unique: true })
  public id!: number;

  @prop()
  public username?: string;

  @prop({
    type: () => BanAction,
    default: [],
    _id: false
  })
  public bans!: [BanAction];

  constructor(user: User, ban: BanAction) {
    super();
    return new BannedUserModel({
      id: user.id,
      username: user.username,
      bans: [ban]
    });
  }

  toIdString() {
    return `@${this.username}(${this.id})`;
  }
}

export const BannedUserModel = getModelForClass(BannedUser);
