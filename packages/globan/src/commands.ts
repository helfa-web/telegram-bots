import { BotContext } from '#middlewares/context';
import { createLogger } from '#root/logging';
import { Bot } from 'grammy';
import { BotCommand as BotCommand } from 'grammy/out/platform';

const log = createLogger('commands');

export let botCommands: Command[];

export class Command implements BotCommand {
  command: string;
  description: string;
  help: string;

  get api(): BotCommand {
    return {
      command: this.command,
      description: this.description
    };
  }

  constructor(data: Command) {
    this.command = data.command;
    this.description = data.description;
    this.help = data.help;
  }
}

try {
  log.info('Loading commands');
  // add command descriptions to this file - /src/bot-commands.json
  botCommands = require('#root/bot-commands.json').map(
    (cmd: Command) => new Command(cmd)
  );
} catch (err) {
  log.fatal(err);
  log.trace(err.stack);
  process.exit(1);
}

export const setBotCommands = (bot: Bot<BotContext>) => {
  log.info('Registering commands');
  bot.api.setMyCommands(botCommands);
};

export const getCommand = (command: string) =>
  botCommands.find((cmd) => cmd.command === command);
