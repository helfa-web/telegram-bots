import { groups } from '#features/groups';
import { start } from '#features/start';
import { ban } from '#features/ban';
import { BotContext, setupBotContext } from '#middlewares/context';
import { setupDatabase } from '#models';
import { setBotCommands } from '#root/commands';
import createLogger, { appLogger } from '#root/logging';
import { Bot } from 'grammy';
import env from './env';

const init = async () => {
  if (!env.TOKEN) {
    appLogger.fatal("Can't start without TOKEN");
    process.exit(1);
  }

  await setupDatabase();

  const catchLog = createLogger('catch');

  appLogger.info('Hello...');
  const bot = new Bot<BotContext>(env.TOKEN);

  appLogger.info('Setting up middlewares');
  bot.use(setupBotContext);

  appLogger.info('Setting up features');
  bot.use(start);
  bot.use(groups);
  bot.use(ban);

  // Channel join events of bots to handle adding of myself
  // add default command set
  setBotCommands(bot);

  bot.catch((errHandler) => {
    catchLog.trace(errHandler.error);
    catchLog.trace(errHandler.stack);
  });

  // Start your bot
  bot.start();
};

init();
