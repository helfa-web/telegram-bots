import { BotContext } from '#middlewares/context';
import env from '#root/env';
import { Middleware } from 'grammy';

/**
 * Stops chain when message is not from a private chat
 */
export const privateOnly: Middleware<BotContext> = async (ctx, next) =>
  ctx.msg?.chat?.type !== 'private'
    ? await ctx.reply('This is available in private chats, only.')
    : await next();

/**
 * Stops chain when message is not from env.ADMIN
 */
export const superAdminOnly: Middleware<BotContext> = async (ctx, next) =>
  ctx.from?.id !== env.ADMIN ? undefined : await next();

/**
 * Stops chain when message is not from a supergroup
 */
export const superGroupOnly: Middleware<BotContext> = async (ctx, next) =>
  ctx.chat?.type !== 'supergroup' ? await ctx.reply('This is available in groups, only.'): await next();
