import { Chat } from '#models/chat';
import env from '#root/env';
import { Context, Filter, Middleware } from 'grammy';

export interface BotContext extends Context {
  dbChat: Chat;
  isSuperAdmin: boolean;
  replyWithDevNote: (ctx: BotContext) => void;
}

export type BotCommandContext = Filter<
  BotContext & {
    match?: string | RegExpMatchArray | null;
  },
  ':entities:bot_command'
>;

export const replyWithDevNote = (ctx: BotContext) =>
  ctx.reply(
    'You should not be here... Please leave a message about what happened in @rbeer_dev_grounds'
  );

/**
 * Populuates {BotContext.isSuperAdmin}, based on whether the message
 * came from the super admin id
 */
export const setupBotContext: Middleware<BotContext> = async (ctx, next) => {
  ctx.replyWithDevNote = replyWithDevNote.bind(null, ctx);
  ctx.isSuperAdmin = ctx.from?.id === env.ADMIN;
  return await next();
};
