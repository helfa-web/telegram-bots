import { BotContext } from '#middlewares/context';
import { Filter, Middleware } from 'grammy';

export type ParsedCommandContext = Filter<
  BotContext & {
    match?: string | RegExpMatchArray | null;
    parsedCommand?: ParsedCommand;
  },
  ':entities:bot_command'
>;

export interface ParsedCommand {
  text?: string;
  command?: string;
  option?: string;
  parameters?: string[];
}

/**
 * Populuates {BotContext.isSuperAdmin}, based on whether the message
 * came from the super admin id
 */
export const parseCommand: Middleware<ParsedCommandContext> = async (
  ctx,
  next
) => {
  if (!ctx.msg?.text) {
    return;
  }
  const [command, option, ...parameters] = ctx.msg.text.split(' ');
  ctx.parsedCommand = {
    text: ctx.msg.text,
    command,
    option,
    parameters
  };
  return await next();
};
