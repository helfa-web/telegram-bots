import env from '#root/env';
import chalk from 'chalk';
// We have types for this in /src/types/
// https://github.com/TypeStrong/ts-node/issues/138
// @ts-ignore
import nicelyFormat from 'nicely-format';

type LogFunction = (...messages: any[]) => void;
interface Logger {
  info: LogFunction;
  warn: LogFunction;
  error: LogFunction;
  debug: LogFunction;
  fatal: LogFunction;
  trace: LogFunction;
  extend: Function;
  namespace: string;
}

const time = () => {
  const now = new Date();
  const date = new Date(now.getTime() - now.getTimezoneOffset() * 60000);
  return date.toISOString().replace(/.*T(.*)Z/, '$1');
};

const indentText = (text: string) =>
  text.replace(/^(?!\s+$)/gm, ' '.repeat(13)).trim();

const format = ({
  namespace,
  messages
}: {
  namespace: string;
  messages: string[];
}) =>
  `${chalk.gray(time())} [${namespace}] ${messages
    .map((message) => {
      if (typeof message === 'string') {
        return message;
      }

      return nicelyFormat(message, {
        highlight: true,
        min: true,
        theme: {
          tag: 'cyan',
          content: 'reset',
          prop: 'yellow',
          value: 'green',
          number: 'green',
          string: 'reset',
          date: 'green',
          symbol: 'red',
          regex: 'red',
          function: 'cyan',
          error: 'red',
          boolean: 'yellow',
          label: 'cyan',
          bracket: 'grey',
          comma: 'grey',
          misc: 'grey',
          key: 'cyan'
        }
      });
    })
    .map((text) => indentText(text))}`;

export const createLogger = (namespace: string): Logger => {
  const extend = (extension: string): Logger =>
    createLogger(`${namespace}:${extension}`);

  return {
    extend,
    namespace,
    debug(...messages) {
      console.log(
        format({
          namespace: chalk.yellow(`DEBUG:${namespace}`),
          messages
        })
      );
    },
    info(...messages) {
      console.info(
        format({
          namespace: chalk.cyan(namespace),
          messages
        })
      );
    },
    warn(...messages) {
      console.warn(
        format({
          namespace: chalk.yellow(`WARNING:${namespace}`),
          messages
        })
      );
    },
    error(...messages) {
      console.error(
        format({
          namespace: chalk.red(`ERROR:${namespace}`),
          messages
        })
      );
    },
    fatal(...messages) {
      console.error(
        format({
          namespace: chalk.red(`FATAL:${namespace} !!!`),
          messages
        })
      );
    },
    trace(...messages) {
      console.info(
        format({
          namespace: chalk.red(`TRACE:${namespace}`),
          messages
        })
      );
    }
  };
};

export const appLogger: Logger = createLogger(env.PACKAGE_NAME);

export default (namespace: string): Logger => appLogger.extend(namespace);
