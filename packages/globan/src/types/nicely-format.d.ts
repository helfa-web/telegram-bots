declare module 'nicely-format' {
  interface Options {
    callToJSON?: boolean;
    indent?: number;
    maxDepth?: number;
    min?: boolean;
    plugins?: [];
    printFunctionName?: boolean;
    escapeRegex?: boolean;
    highlight?: boolean;
    theme?: {
      tag: string;
      content: string;
      prop: string;
      value: string;
      number: string;
      string: string;
      date: string;
      symbol: string;
      regex: string;
      function: string;
      error: string;
      boolean: string;
      label: string;
      bracket: string;
      comma: string;
      misc: string;
      key: string;
    };
  }
  function nicelyFormat(message: string, options: Options);
  export default nicelyFormat;
}
