#!/bin/sh

if [ ! -z ${MONGO_GLOBAN_PASSWORD_FILE+x} ]; then
  MONGO_GLOBAN_PASSWORD=$(cat ${MONGO_GLOBAN_PASSWORD_FILE})
fi

if [ ! -z ${TOKEN_FILE+x} ]; then
  TOKEN=$(cat ${TOKEN_FILE})
  export TOKEN
fi

export MONGO=mongodb://${MONGO_GLOBAN_USERNAME}:${MONGO_GLOBAN_PASSWORD}@mongo:27017/${MONGO_INITDB_DATABASE}

yarn start
